#ifndef CONTROLER_H
#define CONTROLER_H

#include <QObject>
#include <QtNetwork>
#include <QString>
#include <QMessageBox>
#include "login_dialog.h"
#include "user_dialog.h"
#include "transmition_code.h"


class Controler : public QObject
{
    Q_OBJECT

public:
    static Controler& server_instance();
    ~Controler();
    void call(QString address, int port);
    
signals:
    
private slots:
    void get_data();
    void display_error(QAbstractSocket::SocketError);
    void display_login();


private:
    Controler();
    Controler(const Controler &);
    Controler& operator=(const Controler&);

    QTcpSocket *server;
    Login_dialog *login_controler;
    User_dialog *user_controler;
    int id_code;
    void login(QStringList parts);
    void send(QStringList parts);
    void _register(QStringList parts);
    void _add(QStringList parts);


};

#endif // CONTROLER_H
