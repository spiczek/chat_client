#include "user_dialog.h"
#include "edit_dialog.h"


User_dialog::User_dialog(QTcpSocket *server, int id,QString new_name,QString new_surname,QString new_login, QWidget *parent) : QMainWindow(parent), ui(new Ui::User_dialog)
{

    ui->setupUi(this);

    this->setWindowTitle(QString("%1 %2 - %3").arg(new_name).arg(new_surname).arg(new_login));
    this->server = server;
    this->id = id;

    name = new_name;
    surname = new_surname;
    login = new_login;
    add_controler = NULL;
    message_jar = new QList<Message_dialog*>();
    model = new user_dialog_model();
    model->get_from_file(id);

    del_row = -1;

    qDebug()<<"IMIE "<<name<<" nazwisko : "<<surname<<" login "<<login;

    for(int i=0;i<model->size();i++)
    {
        ui->User_list->addItem(model->get_login(i));


    }

    QString s_id ;
    s_id.setNum(id);
    ui->dane->setText("Your id: " + s_id );


    connect(this->ui->Add_button, SIGNAL(clicked()), this, SLOT(add_button_cliced()));

}
void User_dialog::add_contact(int new_id,QString name,QString surname,QString login)
{
    model->add_contact(new_id,name,surname,login);
    ui->User_list->addItem(login);
    model->save_to_file(id);




    for (int i = 0; i < message_jar->size(); i++)
    {
        if (message_jar->at(i)->get_reciver() == new_id)
        {
            if (message_jar->at(i)->get_anonymous())
            {
                qDebug() << "*********anonymous************" <<endl;
                message_jar->at(i)->set_reciver(login);
                message_jar->at(i)->setWindowTitle(QString("%1 Chat").arg(login));
                message_jar->at(i)->set_anonymous(false);
            }
        }
    }



}

User_dialog::~User_dialog()
{
    qDebug() << "******* user dialog destructor ******" << endl;
    this->clear_jar();

    delete ui;
}

void User_dialog::update_message(int reciver_id, QString msg)
{
    Message_dialog *message = open_message(reciver_id);
    message->refresh_messages(msg);
}


Message_dialog* User_dialog::open_message(int reciver)
{
    for (int i = 0; i < message_jar->size(); i++)
    {
        if (message_jar->at(i)->get_reciver() == reciver)
        {
            message_jar->at(i)->show();
            message_jar->at(i)->activateWindow();
            return message_jar->at(i);
        }
    }

    // to do check login details in model
    QString reciver_nick = model->get_login_id(reciver);

    if (reciver_nick.isEmpty())
        reciver_nick = "";

    Message_dialog *message_controler = new Message_dialog(login,this->id, reciver, reciver_nick, server);
    message_controler->show();
    message_jar->push_back(message_controler);
    qDebug() << "added to jar size: " << message_jar->size() << "reciver id: " << reciver;

    return message_controler;
    //usuniecie ze sloika
}

void User_dialog::clear_jar()
{
    int size = message_jar->size();

    for (int i = 0; i < size; i++)
    {
        delete message_jar->at(0);
        message_jar->removeAt(0);

        qDebug() << "dialogs in jar: " << message_jar->size() << endl;
    }

    delete message_jar;
}



void User_dialog::add_button_cliced()
{
    add_controler = new Add_dialog(server, this);

    add_controler->exec();
}


QWidget *User_dialog::add_dialog()
{
     qDebug()<<"weszlo";
    return add_controler;
}


void User_dialog::closeEvent(QCloseEvent *event)
{
     //model->save_to_file(id);
     this->~User_dialog();
}

int User_dialog::_id(int i)
{
    return i ==this->id;
}

bool User_dialog::is_id(int nr)
{
    return model->is_id(nr);
}



void User_dialog::on_pushButton_clicked()
{
    qDebug() << "____________" << ui->User_list->currentIndex().internalId() << endl;



    if(del_row!=-1)
    {
        ui->User_list->takeItem(del_row);

        model->delete_contact(del_row);
        model->save_to_file(id);
        del_row = -1;
    }
}




void User_dialog::on_User_list_itemDoubleClicked(QListWidgetItem *item)
{
    int reciver_row;
    reciver_row = ui->User_list->row(item);

    open_message(model->get_id(reciver_row));
}

void User_dialog::on_User_list_itemClicked(QListWidgetItem *item)
{
    del_row = ui->User_list->row(item);
}

void User_dialog::refresh()
{

    ui->User_list->clear();

     for(int i=0;i< model->size();i++)
         ui->User_list->addItem(model->get_login(i));
}




void User_dialog::on_editbutton_clicked()
{
    //refresh();
    if(del_row!=-1)
    {
        edit_window = new edit_Dialog(model,del_row,this);
        edit_window->exec();
        del_row = -1;
    }
}

void User_dialog::on_User_list_itemSelectionChanged()
{
    qDebug() << "selection" << endl;

}

