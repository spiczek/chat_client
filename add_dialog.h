#ifndef ADD_DIALOG_H
#define ADD_DIALOG_H

#include <QDialog>
#include <QDebug>
#include <QTcpSocket>
#include <QMessageBox>
#include <QCloseEvent>
#include "user_dialog_model.h"

namespace Ui {
class Add_dialog;
}

class Add_dialog : public QDialog
{
    Q_OBJECT
    
public:
    Add_dialog(QTcpSocket *server, QWidget *parent);
    ~Add_dialog();
    
private:
    Ui::Add_dialog *ui;
    QWidget *parent;
    QTcpSocket *server;
    user_dialog_model *model;

private slots:
    void cancel_cliced();
    void ok_cliced();

protected:
    void closeEvent(QCloseEvent *event);
};

#endif // ADD_DIALOG_H
