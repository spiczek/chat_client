#include "message_dialog.h"
#include "ui_message_dialog.h"

Message_dialog::Message_dialog(QString my_login, int id, int reciver_id, QString reciver_login, QTcpSocket *server, QWidget *parent) : QMainWindow(parent), ui(new Ui::Message_dialog)
{
    ui->setupUi(this);


    qDebug() << "message controelr" << endl;

    if (reciver_login.isEmpty())
    {
        reciver_login = QString("anonymous id %1").arg(reciver_id);
        anonymous = true;
    }
    else
        anonymous = false;

    this->setWindowTitle(QString("%1 Chat").arg(reciver_login));

    this->server = server;
    this->id_code = id;
    this->reciver_id = reciver_id;
    this->reciver_login = reciver_login;
    this->user_login = my_login;



    qDebug() << "my id " << QString::number(id) << endl;

    connect(ui->Send_button, SIGNAL(clicked()), this, SLOT(send_message()));
}

Message_dialog::~Message_dialog()
{
    qDebug() << "********** deleting message dialog id: " << this->reciver_id << endl;

    //delete server;
    delete ui;
}

void Message_dialog::refresh_messages(QString data, int i )
{

    /*

    int start = data.indexOf("<");
    int end = data.indexOf(">");
    QString t = data;

    if (start != -1 && end != -1)
    {
        if (start < end)
        {
            qDebug() << "sadf";
            t = data.replace(start, end-start+1, " ");
            qDebug() << t;
        }
        else
        {qDebug() << "++";
            t = data.replace(start, 1, " ");
            t = data.replace(end, 1, " ");
        }
    }

    qDebug() << t << "''''";
    */



    //QString msg = QString("[ %1 ] %2: %3").arg(QTime::currentTime().toString()).arg(sender_id).arg(data);

    QTextCursor cursor(ui->Message_box->textCursor());
    cursor.movePosition(QTextCursor::End, QTextCursor::MoveAnchor);
    ui->Message_box->setTextCursor(cursor);


    ui->Message_box->append("");

    ui->Message_box->setTextColor( QColor("black") );
    ui->Message_box->insertPlainText(QString("[%1] ").arg(QTime::currentTime().toString()));

    ui->Message_box->setTextColor( QColor("blue") );
    if(i == 0)
        ui->Message_box->insertPlainText(QString("%1: ").arg(reciver_login));
    else
        ui->Message_box->insertPlainText(QString("%1: ").arg(user_login));

    ui->Message_box->setTextColor( QColor("black") );
    ui->Message_box->insertPlainText(QString("%1").arg(data));



}


int Message_dialog::get_reciver()
{
    return reciver_id;
}

bool Message_dialog::get_anonymous()
{
    return anonymous;
}

void Message_dialog::set_anonymous(bool b)
{
    anonymous = b;
}

void Message_dialog::set_reciver(QString login)
{
    qDebug() << "setting";
    reciver_login = login;
}

QString Message_dialog::validate(QString message)
{
    int start = message.indexOf("<");
    int end = message.indexOf(">");

    if (start != -1 && end != -1)
    {
        if (start < end)
        {
            message = message.replace(start, end-start+1, " ");
        }
        else
        {
            message = message.replace(start, 1, " ");
            message = message.replace(end, 1, " ");
        }
    }

    return message;
}


void Message_dialog::send_message()
{
    QString message = ui->Message_edit->text();
    this->ui->Message_edit->clear();

    Transmition_code *code = new Transmition_code(1);
    //QByteArray array = QString("1::%1;%2;%3").arg(id_code).arg(reciver_id).arg(message).toUtf8();

    QString v1 = QString("%1").arg(id_code);
    QString v2 = QString("%1").arg(reciver_id);

    message = validate(message);
        server->write(code->encode(v1, v2, message).toUtf8());

            refresh_messages(message,1);

    delete code;

}
