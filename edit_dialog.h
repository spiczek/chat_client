#ifndef EDIT_DIALOG_H
#define EDIT_DIALOG_H

#include <QDialog>
#include <QWidget>
#include "ui_edit_dialog.h"
//#include "user_dialog.h"

#include "user_dialog_model.h"


namespace Ui {
class edit_Dialog;
}

class edit_Dialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit edit_Dialog(user_dialog_model *model, int row, QWidget *parent = 0);
    ~edit_Dialog();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();


    
private:
    Ui::edit_Dialog *ui;
    int row;
    user_dialog_model *model;
    QWidget *parent;
};

#endif // EDIT_DIALOG_H
