#-------------------------------------------------
#
# Project created by QtCreator 2012-12-07T21:34:33
#
#-------------------------------------------------

QT       += core
QT       += network
QT       += gui

TARGET = Chat_client
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    login_dialog.cpp \
    controler.cpp \
    register_dialog.cpp \
    message_dialog.cpp \
    user_dialog.cpp \
    add_dialog.cpp \
    user_dialog_model.cpp \
    edit_dialog.cpp \
    transmition_code.cpp

HEADERS += \
    login_dialog.h \
    controler.h \
    register_dialog.h \
    message_dialog.h \
    user_dialog.h \
    add_dialog.h \
    user_dialog_model.h \
    edit_dialog.h \
    transmition_code.h

FORMS += \
    login_dialog.ui \
    test.ui \
    user_dialog.ui \
    register_dialog.ui \
    message_dialog.ui \
    add_dialog.ui \
    edit_dialog.ui
