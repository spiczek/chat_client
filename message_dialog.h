#ifndef MESSAGE_DIALOG_H
#define MESSAGE_DIALOG_H

#include <QMainWindow>
#include <QTcpSocket>
#include <QTime>
#include "transmition_code.h"


namespace Ui {
class Message_dialog;
}

class Message_dialog : public QMainWindow
{
    Q_OBJECT

public:
    Message_dialog(QString my_login,int id, int reciver_id, QString reciver_login, QTcpSocket *server, QWidget *parent = 0);
    ~Message_dialog();
    void refresh_messages(QString data, int i=0);
    int get_reciver();
    bool get_anonymous();
    void set_anonymous(bool b);
    void set_reciver(QString login);

private:
    Ui::Message_dialog *ui;
    int id_code;
    int reciver_id;
    QString reciver_login,user_login;
    QTcpSocket *server;
    bool anonymous;
    QString validate(QString message);


private slots:
    void send_message();

};

#endif // MESSAGE_DIALOG_H
