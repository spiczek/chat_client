#include "register_dialog.h"
#include "ui_register_dialog.h"
#include "login_dialog.h"

Register_dialog::Register_dialog(QTcpSocket *server, QWidget *parent) : QDialog(parent), ui(new Ui::Register_dialog)
{
    ui->setupUi(this);
    this->parent = parent;
    this->server = server;

    //((Login_dialog*)parent)->close();

    connect(ui->Cancel_button, SIGNAL(clicked()), this, SLOT(cancel_cliced()));
    connect(ui->Ok_button, SIGNAL(clicked()), this, SLOT(ok_cliced()));
}

Register_dialog::~Register_dialog()
{
    qDebug() << "******** register dialog destructor *******" << endl;
    delete ui;
}

void Register_dialog::cancel_cliced()
{
    this->~Register_dialog();
}

void Register_dialog::ok_cliced()
{
    QString name = ui->Name_edit->text();
    QString surname = ui->Surname_edit->text();
    QString login = ui->Login_edit->text();
    QString pass = ui->Password_edit->text();

    if (pass == "" || login == "" || name == "" || surname == "")
    {
        QMessageBox::information(this, "Register", "Please insert register details.");
    }
        else
        {

            Transmition_code *code = new Transmition_code(2);

            //QByteArray array = QString("2::%1;%2;%3;%4").arg(name).arg(surname).arg(login).arg(pass).toUtf8();
            server->write(code->encode(name, surname, login, pass).toUtf8());
            delete code;
        }

}

void Register_dialog::closeEvent(QCloseEvent *event)
{
    this->~Register_dialog();
}
