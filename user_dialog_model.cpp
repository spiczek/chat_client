#include "user_dialog_model.h"

#include <QFile>
#include <QDebug>
#include <QDebug>
#include <QListWidgetItem>
#include <QFile>
#include <QTextStream>

user_dialog_model::user_dialog_model()
{
}

void user_dialog_model::add_contact(int new_id,QString new_name,QString new_surname,QString new_login)
{
     contact new_c;

     new_c.id = new_id;
     new_c.name = new_name;
     new_c.surname = new_surname;
     new_c.login = new_login;
     this->contact_list.push_back(new_c);
}

QString user_dialog_model::get_login(int i)
{

return contact_list[i].login;
}

int user_dialog_model::size()
{
 return contact_list.size();
}

void user_dialog_model::delete_contact(int nr)
{


     contact_list.erase(contact_list.begin() + nr );
}

void user_dialog_model::set_login(int i,QString login)
{
      contact_list[i].login = login;
}


void user_dialog_model::save_to_file(int user)
{
    qDebug() << "Saving to file" << endl;

    QDir dir("contacts");
    if (!dir.exists()) {
        dir.mkpath(".");
    }

     QString prefix = "contacts/ini";
     QString postfix = ".txt";
     QString middle ;
     middle.setNum(user);
     QFile file(prefix + middle + postfix);
     qDebug()<<prefix + middle + postfix;
     if (!file.open( QIODevice::Truncate | QIODevice::WriteOnly| QIODevice::Text))
         qDebug()<<"error file not found";

     QTextStream out(&file);
     for(unsigned int i=0;i<contact_list.size();i++)
     {
         out<<contact_list[i].id<<":"<<contact_list[i].name<<":"<<contact_list[i].surname<<":"<<contact_list[i].login<<"\n";
     }

file.close();
}
int user_dialog_model::get_id(int i)
{
     return contact_list[i].id;
}
QString user_dialog_model::get_login_id(int id_)
{
     for(unsigned int i=0;i<contact_list.size();i++)
     {
         if(contact_list[i].id == id_)
             return contact_list[i].login;
     }

     return "";
}

bool user_dialog_model::is_id(int nr)
{
     for(unsigned int i=0;i<contact_list.size();i++)
     {
         if(contact_list[i].id==nr)
             return true;
     }
     return false;
}

void user_dialog_model::get_from_file(int user)
{
    qDebug() << "Loading from file" << endl;


     QString prefix = "contacts/ini";
     QString postfix = ".txt";
     QString middle ;
     middle.setNum(user);
     QFile file(prefix + middle + postfix );
     if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
         qDebug()<<"error file not found";

     QList<QString> parts;
     QTextStream in(&file);
     QString line = in.readLine();

     parts = line.split(":");


     while (!line.isNull())
     {
        int us_id;
        QString us_login,us_name,us_surname;
        us_id = parts.first().toInt();
        parts.pop_front();

        us_name = parts.first();
        parts.pop_front();

        us_surname = parts.first();
        parts.pop_front();

        us_login = parts.first();
        parts.pop_front();

        line = in.readLine();

        if(!line.isNull())
            parts = line.split(":");

        this->add_contact(us_id,us_name,us_surname,us_login);

     }
      file.close();



}
