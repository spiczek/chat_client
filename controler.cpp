#include "controler.h"



Controler& Controler::server_instance()
{
    static Controler instance;
    return instance;
}

Controler::Controler()
{
    login_controler = NULL;
    user_controler = NULL;
}

Controler::~Controler()
{
    qDebug() << "controler destructor";
    server->close();
    //delete server;

    //if (login_controler != NULL)
        //delete login_controler;

    //if (user_controler != NULL)
        //delete user_controler;

}

void Controler::call(QString address, int port)
{

    server = new QTcpSocket();

    connect(server, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(display_error(QAbstractSocket::SocketError)));
    connect(server, SIGNAL(readyRead()), this, SLOT(get_data()));
    connect(server, SIGNAL(connected()), this, SLOT(display_login()));


    server->connectToHost(address, port);
}

void Controler::display_login()
{
    ////////was in constructor
    login_controler = new Login_dialog(server);
    login_controler->show();


}

void Controler::get_data()
{

    QByteArray data = server->readAll();

    //data.split("*").at(0)

    //QList<QString> e = t.split("*");

    //if (e.size() > 1)
        //t = e.at(0);

    //QList<QString> parts = t.split(":");


    //qDebug() << "_________________data: " << data << endl;
    //if (parts.size() == 1)
    //{
        //qDebug() << "wrong args" << endl;
        //return;
    //}


    Transmition_code *code = new Transmition_code(data.split('*').at(0));

    switch (code->get_function())
    {
        case 0:
            login(code->get_args());
        break;

        case 1:
            send(code->get_args());
        break;

        case 2:
            _register(code->get_args());
        break;

        case 3:
            _add(code->get_args());
        break;

        default:
            qDebug() << "wrong function name" << endl;
    }

    delete code;

    qDebug() << "___________________________message " << data << endl;
}

void Controler::login(QStringList parts)
{

    qDebug() << "(((((((((((((((((((((((((((((((((((((((";

    if (parts.size() == 1)
    {
        qDebug() << "wrong args" << endl;
       return;
    }

    if (parts.at(0) == "0")
    {
        id_code = parts.at(1).toInt();


        qDebug() << "loged as; " << id_code << endl;

        user_controler = new User_dialog(server, id_code,parts.at(2),parts.at(3),parts.at(4));
        //user_controler->deleteLater();
        user_controler->show();
        login_controler->close();

        //connect(user_controler, SIGNAL() ,this, SLOT(t(int)));
    }
    else
    {
        QMessageBox::information(login_controler, "Login", parts.at(1));
    }
}

void Controler::send(QStringList parts)
{
    //QList<QString> parts = data.split(";");

    //qDebug() << "^^^^^^^^^^reciving from send" << data << endl;

    if (parts.size() == 1)
    {
        qDebug() << "wrong args" << endl;
        return;
    }

    if (parts.at(0) == "0")
    {
        qDebug() << "tam";
        user_controler->update_message(parts.at(1).toInt(), parts.at(2));
        //Message_dialog *message = this->user_controler->display_message(this->id_code, parts.at(1).toInt());
        //message->refresh_messages(parts.at(1).toInt(), parts.at(2));
    }
    else if (parts.at(0) == "2")
    {
        qDebug() << "++++++++++++++++++++++++++";
        //QMessageBox::information(user_controler, "User", parts.at(1));
    }
}

void Controler::_register(QStringList parts)
{
    //QList<QString> parts = data.split(";");

    //qDebug() << data << endl;

    if (parts.size() == 1)
    {
        qDebug() << "wrong args" << endl;
        return;
    }

    if (parts.at(0) == "0")
    {
        QMessageBox::information(login_controler->register_dialog(), "Register", "Congratulations you have registred successfully, you can login now.");
        login_controler->register_dialog()->close();
        login_controler->show();
    }
    else if (parts.at(0) == "1")
    {
        QMessageBox::information(login_controler->register_dialog(), "Register", parts.at(1));
    }
}

void Controler::_add(QStringList parts)
{
qDebug() << "***** add *****" << endl;
    //QList<QString> parts = data.split(";");

    //qDebug() << data << endl;

    if (parts.size() == 1)
    {
        qDebug() << "wrong args" << endl;
        return;
    }

    if (parts.at(0) == "0")
    {
        //QMessageBox::information(user_controler->add_dialog(), "Add", "Congratulations you have added a friend successfully.");

        parts.pop_front();
        //parts.pop_front();

        int id = parts.front().toInt();
        parts.pop_front();

        QString date[3];
        for(int i=0;i<3;i++)
        {
            date[i] = parts.front();
            parts.pop_front();
        }
        user_controler->add_contact(id,date[0],date[1],date[2]);

        user_controler->add_dialog()->close();
        user_controler->show();

    }
    else if (parts.at(0) == "1")
    {
        QMessageBox::information(user_controler->add_dialog(), "Add", parts.at(1));
    }
}

void Controler::display_error(QAbstractSocket::SocketError socket_error)
{

    switch (socket_error)
    {
        case QAbstractSocket::RemoteHostClosedError:
        break;

        case QAbstractSocket::HostNotFoundError:
        QMessageBox::information(login_controler, "Login", "The host was not found. Please check the "
                                 "host name and port settings.");

        break;

        case QAbstractSocket::ConnectionRefusedError:
        QMessageBox::information(login_controler, "Login", "The connection was refused by the peer. "
                                 "Make sure that the server is running, "
                                 "and check that the host name and port "
                                 "settings are correct.");


        break;

        default:
        QMessageBox::information(login_controler, "Login", server->errorString());

    }

}




