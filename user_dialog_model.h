#ifndef USER_DIALOG_MODEL_H
#define USER_DIALOG_MODEL_H
#include <vector>
#include <QString>
#include <QFile>
#include <QDir>
#include <QDebug>
#include <QListWidgetItem>
#include <QTextStream>

class user_dialog_model
{
    struct contact
    {
        int id;
        QString name;
        QString surname;
        QString login;
    };

    std::vector<contact> contact_list;
public:

    user_dialog_model();
    void add_contact(int new_id,QString new_name,QString new_surname,QString new_login);
    QString get_login(int i);
    void get_from_file(int user);
     int size();
     void delete_contact(int nr);
     void save_to_file(int user);
     int get_id(int i);
     QString get_login_id(int id_);
     void set_login(int i, QString login);
     bool is_id(int nr);

};

#endif // USER_DIALOG_MODEL_H
