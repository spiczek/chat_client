#ifndef TRANSMITION_CODE_H
#define TRANSMITION_CODE_H

#include <QDebug>
#include <QString>
#include <QByteArray>
#include <QStringList>

class Transmition_code
{
public:
    Transmition_code(QByteArray array);
    Transmition_code(int id);
    void set_login_ptr(void (*login_ptr)(QString));
    void use_it(QString);

    int get_function();
    QStringList get_args();

    QString error_encode(QString error);

    QString encode(QString v1, QString v2, QString v3, QString v4);
    QString encode(QString v1, QString v2, QString v3);
    QString encode(QString v1, QString v2);
    QString encode(QString v1);
    QString encode();

    //int login_code() { return LOGIN_FUNCTION; }
    //const int send_code() { return SEND_FUNCTION; }
    //const int register_code() { return REGISTER_FUNCTION; }
    //const int add_code() { return ADD_FUNCTION; }

    //login QString("0::%1;%2").arg(login).arg(pass).toUtf8();
    //send QString("1::%1;%2;%3").arg(id_code).arg(reciver_id).arg(message).toUtf8();
    //register QString("2::%1;%2;%3;%4").arg(name).arg(surname).arg(login).arg(pass).toUtf8();
    //add QString("3::%1;").arg(id).toUtf8();

    //QString encode(QString id);
    //login QString("0::%1;%2").arg(login).arg(pass).toUtf8();


private:


    static const int LOGIN_FUNCTION = 0;
    static const int SEND_FUNCTION = 1;
    static const int REGISTER_FUNCTION = 2;
    static const int ADD_FUNCTION = 3;
    static const QString ARGS_SEPARATOR;
    static const QString FUNCTION_SEPARATOR;
    static const QString FUNCTION_SEPARATOR2;
    static const QString ERROR;
    static const QString NO_ERROR;
    void (*login_ptr)(QString);
    int _function;
    QStringList _args;


};



#endif // TRANSMITION_CODE_H
