#include "login_dialog.h"
#include "ui_login_dialog.h"
#include <QtCore>

Login_dialog::Login_dialog(QTcpSocket *server, QWidget *parent) : QDialog(parent), ui(new Ui::Login_dialog)
{
    ui->setupUi(this);

    this->server = server;

    connect(ui->Ok_button, SIGNAL(clicked()), this, SLOT(ok_cliced()));
    connect(ui->Register_link, SIGNAL(clicked()), this, SLOT(register_user()));
}

Login_dialog::~Login_dialog()
{
    qDebug() << "***** deleting login dialog *****" << endl;
    delete ui;
}


void Login_dialog::ok_cliced()
{
    QString login = ui->user_name_text->text();
    QString pass = ui->password_text->text();


    if (pass == "" || login == "")
    {
        QMessageBox::information(this, "Login", "Please insert login and password.");

    }
        else
        {
            Transmition_code *code = new Transmition_code(0);
            //QByteArray array = QString("0::%1;%2").arg(login).arg(pass).toUtf8();
            server->write(code->encode(login, pass).toUtf8());
            delete code;
        }

}

void Login_dialog::register_user()
{


    register_controler = new Register_dialog(server, this);

    register_controler->exec();

    //this->close();
}

QWidget *Login_dialog::register_dialog()
{
    return register_controler;
}

void Login_dialog::closeEvent(QCloseEvent *event)
{
    this->~Login_dialog();
}
