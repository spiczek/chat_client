#ifndef LOGIN_DIALOG_H
#define LOGIN_DIALOG_H

#include <QDialog>
#include "QDebug"
#include <QMessageBox>
#include <QTcpSocket>
#include <QCloseEvent>
#include "register_dialog.h"


namespace Ui {
class Login_dialog;
}

class Login_dialog : public QDialog
{
    Q_OBJECT
    
public:
    Login_dialog(QTcpSocket *server, QWidget *parent = 0);
    ~Login_dialog();
    QWidget *register_dialog();

    
private:
    Ui::Login_dialog *ui;
    QTcpSocket *server;
    Register_dialog *register_controler;



private slots:
    void ok_cliced();
    void register_user();

signals:
    void login_args(QString login, QString pass);
    void signal_register();

protected:
    void closeEvent(QCloseEvent *event);

};

#endif // LOGIN_DIALOG_H
