#ifndef USER_DIALOG_H
#define USER_DIALOG_H

#include <QDialog>
#include <QModelIndex>
#include <QTcpSocket>
#include <QList>
#include <QCloseEvent>
#include "message_dialog.h"
#include "add_dialog.h"
#include "user_dialog_model.h"
#include <QListWidgetItem>
#include "edit_dialog.h"
#include "ui_user_dialog.h"
//#include <QAction>


namespace Ui {
class User_dialog;
}

class User_dialog : public QMainWindow
{
    Q_OBJECT

public:
    User_dialog(QTcpSocket *server, int id, QString new_name , QString new_surname, QString new_login , QWidget *parent = 0);
    ~User_dialog();
    void update_message(int reciver_id, QString msg);
    QWidget *add_dialog();
    void add_contact(int new_id,QString name,QString surname,QString login);
    void refresh();
    bool is_id(int nr);
    int _id(int i);


private slots:
  //  void on_User_list_doubleClicked(const QModelIndex &index);
    void add_button_cliced();

    void on_pushButton_clicked();



   // void on_User_list_itemClicked(QListWidgetItem *item);

    void on_User_list_itemDoubleClicked(QListWidgetItem *item);

    void on_User_list_itemClicked(QListWidgetItem *item);



    void on_editbutton_clicked();

    void on_User_list_itemSelectionChanged();



private:
    Ui::User_dialog *ui;
    edit_Dialog *edit_window;
    int id,del_row;
    user_dialog_model *model;
    int reciver_id;
    QTcpSocket *server;
    QList<Message_dialog*> *message_jar;
    Add_dialog *add_controler;
    Message_dialog* open_message(int reciver);
    void clear_jar();
    QString login,name,surname;

protected:
    void closeEvent(QCloseEvent *event);
};

#endif // USER_DIALOG_H
