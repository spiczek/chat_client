#include "add_dialog.h"
#include "ui_add_dialog.h"
#include "user_dialog.h"

Add_dialog::Add_dialog(QTcpSocket *server, QWidget *parent) : QDialog(parent), ui(new Ui::Add_dialog)
{
    ui->setupUi(this);

    this->parent = parent;
    this->server = server;

    connect(ui->Cancel_button, SIGNAL(clicked()), this, SLOT(cancel_cliced()));
    connect(ui->Ok_button, SIGNAL(clicked()), this, SLOT(ok_cliced()));
}

Add_dialog::~Add_dialog()
{
    qDebug() << "***** deleting add dialog *****" << endl;
    delete ui;
}

void Add_dialog::cancel_cliced()
{
    qDebug()<<"okienko dodawania destrkutor";
    this->~Add_dialog();
}

void Add_dialog::ok_cliced()
{
    QString id = ui->Id_edit->text();

    if (id == "")
    {
        QMessageBox::information(this, "Add", "Please insert user id.");
    }
    else if(((User_dialog*)parent)->is_id(id.toInt()))
    {
        QMessageBox::information(this, "Add", "User with this id already exist in your contact list.");
    }
    else if(((User_dialog*)parent)->_id(id.toInt()))
    {
        QMessageBox::information(this, "Add", "You can't add yourself to cantact");
    }
    else
    {
            Transmition_code *code = new Transmition_code(3);

            //QByteArray array = QString("3::%1;").arg(id).toUtf8();

            //qDebug() << array << endl;
            //qDebug() << code->encode(id) << endl;

            server->write(code->encode(id).toUtf8());

            delete code;
    }
}

void Add_dialog::closeEvent(QCloseEvent *event)
{
    this->~Add_dialog();
}
