#ifndef REGISTER_DIALOG_H
#define REGISTER_DIALOG_H

#include <QDialog>
#include <QDebug>
#include <QTcpSocket>
#include <QCloseEvent>
#include "transmition_code.h"

namespace Ui {
class Register_dialog;
}

class Register_dialog : public QDialog
{
    Q_OBJECT
    
public:
    Register_dialog(QTcpSocket *server, QWidget *parent);
    ~Register_dialog();
    
private:
    Ui::Register_dialog *ui;
    QWidget *parent;
    QTcpSocket *server;

private slots:
    void cancel_cliced();
    void ok_cliced();

protected:
    void closeEvent(QCloseEvent *event);
};

#endif // REGISTER_DIALOG_H
