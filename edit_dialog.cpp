#include "edit_dialog.h"
#include "user_dialog.h"


edit_Dialog::edit_Dialog(user_dialog_model *model, int row, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::edit_Dialog)
{
    ui->setupUi(this);

    this->model = model;
    this->row = row;
    this->parent = parent;
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_2_clicked()));
}

edit_Dialog::~edit_Dialog()
{
    delete ui;
}



void edit_Dialog::on_pushButton_clicked()
{
    QString new_login = ui->lineEdit->text();
    if(new_login == "")
    {
        this->~edit_Dialog();
        return;

    }
    model->set_login(row,new_login);
    ((User_dialog*)parent)->refresh();
    this->~edit_Dialog();
}

void edit_Dialog::on_pushButton_2_clicked()
{
   // emit refresh();
   // parent->refresh();
    this->~edit_Dialog();
}

